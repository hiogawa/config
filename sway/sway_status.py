import io
import re
import sys
import subprocess
import time

# TODO: pulseaudio dbus ?
def get_volume():
    s = subprocess.getoutput('amixer get Master')
    m = re.search('Front\ Left\:\ Playback\ \d*\ \[(\d*)\%\] \[(\w*)\]', s)
    if not m or m.group(2) == 'off':
        return None
    else:
        return m.group(1)


# TODO: networkmanager dbus ?
def get_network():
    command = "nmcli c s --active | awk '{ if (NR != 1 && $3 !~ /bridge/) print $1 }'"
    return subprocess.getoutput(command)


def get_time():
    return time.strftime('%Y-%m-%d %H:%M:%S')


def get_battery():
    s = io.open('/sys/class/power_supply/BAT1/uevent').read()
    m = re.search('POWER_SUPPLY_CAPACITY=(\d+)', s)
    return m and m.group(1)

def get_charging():
    s = io.open('/sys/class/power_supply/BAT1/uevent').read()
    m = re.search('POWER_SUPPLY_STATUS=(\w+)', s)
    if m and m.group(1) == 'Charging':
        return "🔌"
    else:
        return "🔋"


# cf. http://unicode.org/emoji/charts/full-emoji-list.html
#     https://www.archlinux.org/packages/extra/any/noto-fonts-emoji/
def get_all():
    return "  ".join([
        "🔉:{}%".format(get_volume() or '---'),
        "📶:{}".format(get_network() or '---'),
        "{}:{}%".format(get_charging(), get_battery()),
        "⏰:{} ".format(get_time())
    ])


def main():
    while True:
        sys.stdout.write(get_all() + "\n")
        sys.stdout.flush()
        time.sleep(1)


if __name__ == '__main__':
    main()
