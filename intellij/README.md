Setup symlinks

- `bash setup.sh`

How to download the configuration

- Go "File -> Export Settings" and export 'settings.zip' under this directory
- Run `unzip -o settings.zip`

How to update the configuration

- Run `rm settings.zip; zip -r settings *`
- Go "File -> Import Settings" and select 'settings.zip'
