#!/bin/bash

# Create symlinks to "${HOME}"/.IdeaIC<version>/config
# cf. https://github.com/JetBrains/intellij-community/blob/master/platform/build-scripts/resources/linux/Install-Linux-tar.txt

DEST_DIR="${HOME}/.IdeaIC2019.1/config"

FILES=(
  keymaps/Personal.xml
  options/colors.scheme.xml
  options/editor.xml
  options/keymap.xml
  options/ui.lnf.xml
)

for file in ${FILES[@]}; do
    ln -sf "${PWD}/${file}" "${DEST_DIR}/${file}"
done
