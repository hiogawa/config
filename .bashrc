export GTK_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
export QT_IM_MODULE=ibus

# Customized prompt
export PS1='$ '

export EDITOR="nano"

export PATH="$HOME/.local/bin:$PATH"

# CapsLock as Control
# $ sudo dumpkeys | mydefault.map
# $ (edit mydefault.map with 'keycode 58 = Control' and save as caps_as_ctrl.map)
function _caps_as_ctrl() {
    sudo loadkeys ~/code/hiogawa/config/caps_as_ctrl.map
}

# Shorthand of emacsclient
# Usage:
# $ ec <path>
# $ ec <path> tty
function _ec() {
    case $2 in
	tty ) emacsclient -nw $1 ;;
	*   ) emacsclient --no-wait --create-frame $1 ;;
    esac
}

# Start desktop environment
# https://wiki.archlinux.org/index.php/Xinit
# https://wiki.archlinux.org/index.php/GNOME
# https://github.com/SirCmpwn/sway/wiki#input-configuration
# https://github.com/way-cooler/way-cooler/blob/master/way-cooler.desktop
# https://wiki.archlinux.org/index.php/KDE#Manual (Not working ??)
function _de() {
    case $1 in
        gnome-x  ) startx gnome-session ;;
        gnome-wl ) XDG_SESSION_TYPE=wayland dbus-run-session gnome-session ;;
        kde-x  ) startx /usr/sbin/startkde ;;
        kde-wl ) startplasmacompositor ;;
        sway     ) XDG_SESSION_TYPE=wayland \
                   WLC_REPEAT_DELAY=300 \
		   WLC_REPEAT_RATE=30 \
		   XKB_DEFAULT_OPTIONS=ctrl:nocaps \
		   sway --debug >> $XDG_RUNTIME_DIR/sway.log 2>&1 ;;
        way-cooler ) XDG_SESSION_TYPE=wayland \
                     WLC_REPEAT_DELAY=300 \
		     WLC_REPEAT_RATE=30 \
		     XKB_DEFAULT_OPTIONS=ctrl:nocaps \
		     dbus-launch --exit-with-session way-cooler ;;
    esac
}

# https://github.com/falconindy/asp
function _asp() {
    local DIR=~/code/arch/asp
    cd $DIR
    asp export $1
    cd -
}

function _dr() {
    if type docker > /dev/null; then
        docker run -v $PWD:/app -it --rm $1 ${2:sh}
    fi
}

function _docker_gc() {
    echo "remove containers ..."
    docker ps -aq -f status=exited | xargs -r docker rm

    echo "remove volumes ..."
    docker volume ls -q -f dangling=true | grep ".\{64\}" | xargs -r docker volume rm

    echo "remove images ..."
    docker images -aq -f dangling=true | xargs -r docker rmi
}

# rbenv: https://github.com/rbenv/rbenv
function _setup_rbenv() {
    export RBENV_ROOT="${HOME}/code/installed/rbenv"
    export PATH="${RBENV_ROOT}/bin:$PATH"
    eval "$(rbenv init -)"
}

# nvm: https://github.com/nvm-sh/nvm
function _setup_nvm() {
    export NVM_DIR=$(readlink -f $HOME/code/installed/nvm)
    . "$NVM_DIR/nvm.sh"
}

# rustup: https://github.com/rust-lang-nursery/rustup.rs
function _setup_rustup() {
    . $HOME/.cargo/env
}

# depot_tools for chromium build https://chromium.googlesource.com/chromium/src/+/master/docs/linux_build_instructions.md
function _setup_depot_tools() {
    export PATH="$PATH:$HOME/code/others/depot_tools"
    _setup_python2
}


# https://wiki.archlinux.org/index.php/Python
function _setup_python2() {
    mkdir -p $XDG_RUNTIME_DIR/python_bin
    ln -sf /usr/bin/python2 $XDG_RUNTIME_DIR/python_bin/python
    ln -sf /usr/bin/python2-config $XDG_RUNTIME_DIR/python_bin/python-config
    export PATH="$XDG_RUNTIME_DIR/python_bin:$PATH"
}

function _setup_java() {
    . /etc/profile.d/jre.sh
    export JAVA_HOME="/usr/lib/jvm/default"
}

function _setup_coursier() {
    export PATH="$PATH:$HOME/code/installed/coursier"
    export PATH="$PATH:$HOME/.local/share/coursier/bin"
}

function _setup_emscripten() {
    source $HOME/code/others/emsdk/emsdk_env.sh
}

# Example:
# $ _setup_android
# $ # studio.sh
# $ # ./gradlew tasks
# $ sdkmanager --list
# $ avdmanager create avd --package 'system-images;android-24;google_apis;x86_64' --name 'testavd'
# $ emulator -avd testavd
function _setup_android() {
    # export PATH="$HOME/code/others/android/android-studio/bin:$PATH"
    export ANDROID_HOME="$HOME/Android/Sdk"
    export PATH="$ANDROID_HOME/tools/bin:$PATH"
    export PATH="$ANDROID_HOME/emulator:$PATH"
    export PATH="$ANDROID_HOME/platform-tools:$PATH"
    # Some app's build system wants this (e.g. vlc-android)
    export ANDROID_NDK="$ANDROID_HOME/ndk-bundle"
    export ANDROID_SDK="$ANDROID_HOME"
    # Godot requires ANDROID_NDK_ROOT
    export ANDROID_NDK_ROOT="$ANDROID_NDK"
}

function _setup_conda() {
    case $1 in
        on)
            . ~/miniconda3/bin/activate $2
        ;;
        off)
            conda deactivate
        ;;
        ls)
            ~/miniconda3/bin/conda-env list
        ;;
        '')
            _setup_conda on root
        ;;
        *)
            echo ":: Here is the help ..."
            type _setup_conda
        ;;
    esac
}

function _py() {
    _setup_conda on root
    trap "_setup_conda off" RETURN
    python "${@}"
}

# TODO: Implement minimal pipenv by yourself. Only needed features are:
# _venv
#   setup     # virtualenv
#   install   # update requiremtns.txt and pip install
#   uninstall # update requiremtns.txt and pip uninstall
#   shell     # bash + activate
#   run       # bash + activate + exec

# Created with `virtualenv "${HOME}/.venv_global"`
function _setup_venv_global() {
    local venv_dir="${HOME}/.venv_global"
    source "${venv_dir}/bin/activate"
}

# Run "deactivate" to leave virtualenv
function _setup_venv() {
    local venv_dir=".venv"
    if test ! -d "${venv_dir}"; then
        echo -n ":: Virtualenv (${venv_dir}) doesn't exist. Proceed to create it? [Y/n] "
        read -en 1
        case "${REPLY}" in
            y|Y) ;;
            '') echo "" ;;
            *) echo ":: Canceled"; return 1 ;;
        esac
        echo ":: Creating virtualenv at ${venv_dir} ..."
        python -m venv --upgrade-deps "${venv_dir}"
        if test "${?}" -ne "0"; then
            echo ":: Creating virtualenv failed."
            return 1
        fi
    fi
    source "${venv_dir}"/bin/activate
}

# TODO: choose from multiple running instances
function _open_jupyter() {
    _setup_conda on root
    trap "_setup_conda off" RETURN
    local url
    while true; do
        echo ':: Checking if jupyter is already running ...'
        url=$(jupyter notebook list | grep 'code/hiogawa/notes' | awk '{ print $1 }')
        if test -z "${url}" ; then
            echo ':: Try launching jupyter from systemctl ...'
            systemctl --user start jupyter
            sleep 1
        else
            break
        fi
    done
    echo ':: Opening notebook ...'
    xdg-open "${url}" > /dev/null 2>&1
}

function _setup_opam() {
    . /home/hiogawa/.opam/opam-init/init.sh > /dev/null 2> /dev/null || true
}

function _setup_riscv() {
    export RISCV="${HOME}/.riscv-tools"
    export PATH="${RISCV}/bin:$PATH"
}

# My Audio Setup
# - 1. alsa with default pulse backend (aka pulse's alsa emulation)
# - 2. jackd with dummy backend (so that launching/killing jackd doesn't have to deal with hw card resource)
# - 3. alsa_in, alsa_out (hw card as jack ports)
# - 4. module-jack-sink, module-jack-source (pulse as jack ports)
# what cadence is missing
# - alsa_in, alsa_out cannot choose hw card
# - pulse doesn't release alsa card
# - when press stop cadence brutally kills pulse daemon
# so, here this script is what compensates what currently cadence doesn't do.
# NOTE:
# - gnome volume controller changes default-sink's volume
# - Sometimes it happens that headphone output is muted. In that case, fix it manually via alsamixer -c <card-name>.
# cf.
# - http://jackaudio.org/faq/pulseaudio_and_jack.html
# - https://github.com/jackaudio/jackaudio.github.com/wiki/WalkThrough_User_PulseOnJack
# - https://kx.studio/Applications:Cadence
# - https://jackaudio.org/faq/linux_rt_config.html

function _setup_audio() {
    _english
    case $1 in
        status)
            local status_cmds=(
                "pactl info | grep Default"
                "pactl list short sinks"
                "pactl list short sink-inputs"
                "pgrep jackdbus -ax"
                "pgrep jackd -ax"
                "pgrep alsa_in -ax"
                "pgrep alsa_out -ax"
                "amixer -c ${CARD:-0} sget Master | grep Playback"
                "amixer -c ${CARD:-0} sget Speaker | grep Playback"
                "amixer -c ${CARD:-0} sget Headphone | grep Playback"
                "cat /etc/security/limits.d/audio.conf"
                "id"
            )
            for cmd in "${status_cmds[@]}"; do
                printf ":: ${cmd} ::\n"
                eval "${cmd}"
                printf "\n"
            done
        ;;
        rt-config)
            echo ':: Write following entries to /etc/security/limits.d/audio.conf'
            cat <<XXX
@audio   -  rtprio     95
@audio   -  memlock    unlimited
XXX
            if grep -q "^audio:" /etc/group; then
                echo ':: Group "audio" found'
            else
                echo ':: Group "audio" not found. Create by "groupadd audio"'
            fi

            if id | grep -q "(audio)"; then
                echo ":: \"$(id -u -n)\" belongs to group \"audio\""
            else
                echo ":: \"$(id -u -n)\" doesn't belong to group \"audio\". Modify by \"usermod -a -G audio $(id -u -n)\""
            fi
        ;;
        up)
            echo ":: Checking if jackd is running ..."
            if [ -z "$(pgrep jackd -x)" ]; then
                echo ":::: jackd was not running. Starting jackd ..."
                jackd -ddummy -r48000 -p512 1>/dev/null 2>&1 &
            fi

            echo ":: Checking if pulseaudio uses jack as sink ..."
            local jack_sink_idx=$(pactl list short sinks | grep module-jack-sink | cut -f 1)
            if [ -z "${jack_sink_idx}" ]; then
                echo ":::: Jack sink was not found. Loading module-jack-sink in pulseaudio ..."
                pactl load-module module-jack-sink
            fi

            echo ":: Suspending pulseaudio's alsa sink ..."
            local alsa_sink_idx=$(pactl list short sinks | grep alsa | cut -f 1)
            pactl suspend-sink "${alsa_sink_idx}" true

            echo ":: Setting up jack-sink as default in pulseaudio ..."
            jack_sink_idx=$(pactl list short sinks | grep module-jack-sink | cut -f 1)
            pactl set-default-sink "${jack_sink_idx}"

            echo ":: Redirecting existing pulseaudio client to jack-sink ..."
            for idx in $(pactl list short sink-inputs | cut -f 1); do
                pactl move-sink-input "${idx}" "${jack_sink_idx}"
            done

            echo ":: Checking if alsa_in/alsa_out is running ..."
            if [ -z "$(pgrep alsa_in -x)" ]; then
                echo ":::: alsa_in was not running. Starting alsa_in ..."
                alsa_in -d hw:${CARD:-0} 1>/dev/null 2>&1 &
            fi
            if [ -z "$(pgrep alsa_out -x)" ]; then
                echo ":::: alsa_out was not running. Starting alsa_out ..."
                alsa_out -d hw:${CARD:-0} 1>/dev/null 2>&1 &
            fi
            sleep 0.5
            echo ":: Connecting pulseaudio jack sink to alsa_out ..."
            jack_connect 'PulseAudio JACK Sink:front-left' 'alsa_out:playback_1'
            jack_connect 'PulseAudio JACK Sink:front-right' 'alsa_out:playback_2'
        ;;
        down)
            set -x
            local alsa_sink_idx=$(pactl list short sinks | grep alsa | cut -f 1)
            pactl suspend-sink "${alsa_sink_idx}" false
            pactl set-default-sink "${alsa_sink_idx}"
            local sink_inputs=$(pactl list short sink-inputs | cut -f 1)
            for idx in "${sink_inputs}"; do
                pactl move-sink-input "${idx}" "${alsa_sink_idx}"
            done
            pactl unload-module module-jack-sink
            pkill alsa_in -x
            pkill alsa_out -x
            set +x
        ;;
        *) echo ":: Command not found: ${@}";;
    esac
}

function _reset_network() {
    sudo modprobe -r iwlmvm
    sudo modprobe -r iwlwifi
    sudo modprobe -r e1000e
    sudo modprobe e1000e
    sudo modprobe iwlwifi
    sudo modprobe iwlmvm
}

function _monitor_network() {
    # For some reason, it often happens that ipv4 connection stops working even though ipv6 works.
    # So here we monitor such incident by ping and reset automatically.
    echo "[_monitor_network:$(date -Is)] Started monitoring"
    while true; do
        NAME=$(nmcli --get-values NAME,TYPE --colors no c show --active | perl -ne '/(.*?):802-11-wireless/ && print "$1\n"')
        if [ -z "${NAME}" ]; then
            echo "[_monitor_network:$(date -Is)] Active connection not found"
            break
        fi
        if ! ping ipv4.google.com -w 1 -c 1 > /dev/zero 2>&1; then
            echo "[_monitor_network:$(date -Is)] Unsuccessful 'ping ipv4.google.com'. Trying 'nmcli c up ${NAME}'"
            nmcli c up "${NAME}"
        fi
        sleep 2
    done
}

function _ms() {
    man -k '.*' | grep $1
}


# Document manager
#   Usage:
#   $ _docman papers https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5366773/
function _docman() {
    local CATEGORY=${1}
    local URL=${2}
    local DATA_DIR=$(echo ~/code/hiogawa/blob/reading/new)
    local SCRIPT=$(echo ~/code/hiogawa/scratch/misc/document-manager.py)

    if [ -z "${CATEGORY}" -a -z "${URL}" ]; then
        return 1;
    fi

    cd ${DATA_DIR}
    _setup_conda on root
    python3 ${SCRIPT} ${CATEGORY} ${URL}
    _setup_conda off
    cd - > /dev/null
}

function _ytdl() {
    local URLS=(${@})
    local DIR="${HOME}/Music/__tmp__"
    local OPTS=(--extract-audio)

    mkdir -p "${DIR}"
    cd "${DIR}"
    for URL in "${URLS[@]}"; do
        youtube-dl "${OPTS[@]}" "${URL}"
    done
    cd - > /dev/null
}

function _ytdl_audio() {
    # Usage: _ytdl_audio <URL-1> <URL-2> ...
    # Note: URL can be a playlist
    # Dependencies: youtube-dl, atomicparsely

    local dst_dir="$HOME/Music"
    local options=(
        --extract-audio
        --audio-format m4a
        --add-metadata
        --embed-thumbnail
        --output "%(channel)s/%(title)s --- %(id)s.%(ext)s"
    )

    mkdir -p "$dst_dir"
    cd "$dst_dir"
    trap "cd - > /dev/null" RETURN

    for url in "$@"; do
        youtube-dl "${options[@]}" "$url"
    done
}

# Usage:
#   _ffmpeg_convert_format opus *.m4a
function _ffmpeg_convert_format() {
    local OUT_EXT="${1}"
    shift
    for IN_FILE in "${@}"; do
        local OUT_FILE="${IN_FILE%%.*}.${OUT_EXT}"
        ffmpeg -y -hide_banner -i "${IN_FILE}" "${OUT_FILE}"
    done
}

function _update_metadata() {
    local ARTIST="${1}"
    local ALBUM="${2}"
    local TITLE="${3}"
    local IN_FILE="${4}"
    local EXT="${IN_FILE##*\.}"
    local FFMPEG_OPTS=()

    local TMP_OUT=$(mktemp --suffix=.${EXT})

    if [ -n "${ARTIST}" ]; then
        FFMPEG_OPTS+=(-metadata:s:a:0 "ARTIST=${ARTIST}")
    fi
    if [ -n "${ALBUM}" ]; then
        FFMPEG_OPTS+=(-metadata:s:a:0 "ALBUM=${ALBUM}")
    fi
    if [ -n "${TITLE}" ]; then
        FFMPEG_OPTS+=(-metadata:s:a:0 "TITLE=${TITLE}")
    fi

    ffmpeg -v warning -y -hide_banner -i "${IN_FILE}" \
        "${FFMPEG_OPTS[@]}" -codec copy "${TMP_OUT}"

    cp "${TMP_OUT}" "${IN_FILE}"
}

# Usage:
# $ _ytdl https://www.youtube.com/playlist?list=PLJ1QPgsrvmL0vfV2_xDCe7BD99C0Lxlj_
# $ _update_metadata_batch 'Q-Tip' 'The Renaissance' ~/Music/__tmp__/*.opus
function _update_metadata_batch() {
    local ARTIST="${1}"
    local ALBUM="${2}"
    shift 2
    local FILES=("${@}")

    echo ":: ARTIST = ${ARTIST}"
    echo ":: ALBUM  = ${ALBUM}"
    echo ":: List of files to update"
    for FILE in "${FILES[@]}"; do
        echo "${FILE}"
    done

    echo -n ":: Proceed to update? [y/n]"
    read -en 1
    if [ "${REPLY}" != "y" ]; then
        echo ":: Canceled"
        return 1
    fi

    local I=1
    for FILE in "${FILES[@]}"; do
        printf "(${I}/${#FILES[@]})\t${FILE}\n"
        _update_metadata "${ARTIST}" "${ALBUM}" "" "${FILE}"
        I=$(( I + 1 ))
    done
}

# Usage:
# $ _rename_files_by_metadata ~/Music ~/Music/__tmp__/*.opus
function _rename_files_by_metadata() {
    local out_dir="${1}"; shift 1
    local files=("${@}")
    local out_files=()

    echo ":: Obtaining audio file metadata..."
    for file in "${files[@]}"; do
        local ffprobe_out=$(ffprobe -hide_banner -show_streams "${file}" 2>&1)
        local artist="$(echo "${ffprobe_out}" | perl -ne '/TAG:ARTIST=(.*)/ && print "$1\n";')"
        local album="$(echo "${ffprobe_out}" | perl -ne '/TAG:ALBUM=(.*)/ && print "$1\n";')"
        local title="$(echo "${ffprobe_out}" | perl -ne '/TAG:TITLE=(.*)/ && print "$1\n";')"
        if [ -z "${title}" ]; then
            title="$(basename "${file}")"
            title="${title%%\.*}"
        fi
        out_files+=("${out_dir}/${artist}/${album}/${title}.${files[i]##*\.}")
    done

    echo ":: List of files to update"
    for (( i=0; i<"${#files[@]}"; i++ )); do
        echo "in : ${files[i]##*\/}"
        echo "out: ${out_files[i]}"
    done

    echo -n ":: Proceed to update? [y/n]"
    read -en 1
    if [ "${REPLY}" != "y" ]; then
        echo ":: Canceled"
        return 1
    fi

    for (( i=0; i<"${#files[@]}"; i++ )); do
        mkdir -p "$(dirname "${out_files[i]}")"
        mv "${files[i]}" "${out_files[i]}"
    done
}

# Usage:
# $ _curl_range https://github.com/mozilla/DeepSpeech/releases/download/v0.5.1/deepspeech-0.5.1-models.tar.gz > out.tar.gz
function _curl_range() {
    local URL=$1
    local CHUNK_SIZE=${2:-$(( 2 ** 22 ))} # default 4MiB

    echo ':: Check if server supports "range" header...' 1>&2
    # NOTE: HEAD request is forbidden for some content server, so use GET request with "empty range" here.
    local WHOLE_SIZE=$(\
        curl -i -L -H 'range: bytes=0-0' "${URL}" 2>/dev/null | \
        ruby -ne 'm = $_.strip.match(/Content-Range: bytes 0-0\/(.*)/); puts m[1] if m')

    if test -z "${WHOLE_SIZE}"; then
      echo ':: "range" header is not supported.' 1>&2
      return 1
    else
      echo ":: Available range is: ${WHOLE_SIZE}"  1>&2
      echo ":: Starting request..."  1>&2
      local NUM_LOOPS=$(( "${WHOLE_SIZE}" / "${CHUNK_SIZE}" ))
      local START
      local END
      for (( i=0; i<=$NUM_LOOPS; i++ )); do
        START=$(( $i * $CHUNK_SIZE ))
        END=$(( ($i + 1) * $CHUNK_SIZE - 1 ))
        echo ":: Request ${START}-${END} ::"  1>&2
        curl -L -H "range: bytes=${START}-${END}" "${URL}"
        echo 1>&2
      done
    fi
}

# Create new tmux pane with preserving current directory
function _tmux_cd() {
    # if already in tmux, then prompt where to use as current directory
    if test "${TERM}" = "screen"; then
        tmux command-prompt -I "${PWD}" -p "tmux_cd: " 'attach-session -c "%1"'
    else
        tmux attach-session -c "${PWD}" \; new-window
    fi
}

# Force english locale
function _english() {
    if [ $# -eq 0 ]; then
        export LC_ALL=en_US.UTF-8
    else
        LC_ALL=en_US.UTF-8 eval ${@}
    fi
}

# Convinient shortcut for vscode debugger
function _ptvsd() {
    local CODE="import ptvsd; print('Waiting for debugger to attach...'); ptvsd.enable_attach(('localhost', 5678)); ptvsd.wait_for_attach();  print('Debugger attached.'); breakpoint()"
    echo "${CODE}"
    echo -n "${CODE}" | xclip -selection c
}

# Simple notification via notify-send, e.g.
# $ ninja -C build; _notify ninja build finished with status "${?}"
function _notify() {
    local MESSAGE="${@:-_notify}"
    notify-send -a 'Shell (_notify)' "${MESSAGE}"
}

# https://github.com/pyenv/pyenv
function _setup_pyenv() {
    export PYENV_ROOT="${HOME}/code/installed/pyenv"
    export PATH="${PYENV_ROOT}/bin:$PATH"
    eval "$(pyenv init -)"
}

function _setup_pipenv() {
    _setup_pyenv
    export PIPENV_SKIP_LOCK=True
    export PIPENV_SEQUENTIAL=True
    export PIPENV_KEEP_OUTDATED=True
    pipenv shell
}

function _swap_refresh() {
    echo ':: You can use "htop" to check a progress.'
    sudo swapoff -a && sudo swapon -a
}

# e.g.
# $ _whose init
# /usr/bin/init is owned by systemd-sysvcompat 243.78-2
function _whose() {
    pacman -Qo $(type -P "${1}")
}

# e.g.
# $ env | _sort
# COLORTERM=truecolor
# DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus
# DESKTOP_SESSION=gnome
# DISPLAY=:0
# ...
function _sort() {
    ruby -e 'puts STDIN.read.split.sort'
}


function _setup_clangd_vscode() {
    echo ':: Write below to .vscode/settings.json' 1>&2
    cat <<XXX
{
    "clangd.arguments": [ "--compile-commands-dir=$PWD/build" ]
}
XXX
}

function _setup_sourcetrail() {
    local COMMAND="${1}"
    local BASENAME=$(basename "${PWD}")
    local EXAMPLE
    read -r -d '' EXAMPLE <<XXX
<?xml version="1.0" encoding="utf-8" ?>
<config>
    <source_groups>
        <source_group_5c0db2c0-bd8e-4c6b-a941-19893be1d2d7>
            <build_file_path>
                <compilation_db_path>build/compile_commands.json</compilation_db_path>
            </build_file_path>
            <indexed_header_paths>
                <!-- <indexed_header_path>include</indexed_header_path> -->
            </indexed_header_paths>
            <name>C/C++ from Compilation Database</name>
            <status>enabled</status>
            <type>C/C++ from Compilation Database</type>
        </source_group_5c0db2c0-bd8e-4c6b-a941-19893be1d2d7>
    </source_groups>
    <version>8</version>
</config>
XXX
    case "${COMMAND}" in
        run)
            echo "${EXAMPLE}" > "${BASENAME}.srctrlprj"
            sourcetrail index "${BASENAME}.srctrlprj"
        ;;
        help|*)
            echo ":: \"_setup_sourcetrail run\" wil write below to \"${BASENAME}.srctrlprj\" and execute \"sourcetrail index ${BASENAME}.srctrlprj\"" 1>&2
            echo "${EXAMPLE}"
        ;;
    esac
}

# Default cmake initialization (e.g. _cmake_init . -B build)
function _cmake_init() {
    CC=clang CXX=clang++ LDFLAGS=-fuse-ld=lld cmake -G Ninja ${@}
}


function _setup_defaults() {
    _setup_nvm
}
