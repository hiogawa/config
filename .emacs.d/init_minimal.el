;; Less ui component
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode t)

;; No auto save
(auto-save-mode -1)
(setq make-backup-files nil)

;; Misc
(setq column-number-mode t)
(show-paren-mode 1)
(electric-pair-mode 1)
(setq electric-pair-pairs '((34 . 34)))
(setq-default indent-tabs-mode nil)
(set-default 'truncate-lines t)

;; Common User Access mode
(cua-mode t)
(setq cua-enable-cua-keys nil)
