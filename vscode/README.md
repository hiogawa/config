On Windows, link files are created by:

```
$ mklink C:\Users\hiogawa-microsoft\AppData\Roaming\Code\User\settings.json C:\Users\hiogawa-microsoft\code\hiogawa\config\vscode\settings.json
$ mklink C:\Users\hiogawa-microsoft\AppData\Roaming\Code\User\keybindings.json C:\Users\hiogawa-microsoft\code\hiogawa\config\vscode\keybindings.json

$ mklink C:\Users\hiogawa-microsoft\AppData\Roaming\"Code - Insiders"\User\settings.json C:\Users\hiogawa-microsoft\code\hiogawa\config\vscode\settings.json
$ mklink C:\Users\hiogawa-microsoft\AppData\Roaming\"Code - Insiders"\User\keybindings.json C:\Users\hiogawa-microsoft\code\hiogawa\config\vscode\keybindings.json
```

cf.

- https://technet.microsoft.com/en-us/library/cc753194.aspx


Cygwin git workaround

```
$ cmd.exe
> mklink C:\cygdrive\c C:\
```

cf

- https://github.com/Microsoft/vscode/blob/master/extensions/git/src/git.ts#L384
- https://cygwin.com/cygwin-ug-net/highlights.html#ov-hi-files
- https://github.com/Microsoft/vscode/issues/7998
- `git.autorefresh` doesn't work
- file-by-file staging doesn't work but region staging works  https://github.com/Microsoft/vscode/blob/master/extensions/git/src/repository.ts#L536


On Linux

```
$ ln -sf $PWD/settings.json ~/.config/Code\ -\ Insiders/User/settings.json
$ ln -sf $PWD/keybindings.json ~/.config/Code\ -\ Insiders/User/keybindings.json
```
