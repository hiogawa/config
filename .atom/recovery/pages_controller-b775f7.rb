class PagesController < ApplicationController
  respond_to :html

  def home
    if request.original_url.to_s.match(Regexp.escape('old.www.odigo.travel'))
      unless authenticate_with_http_basic { |u, p| u == p && u == 'odigoer' }
        return request_http_basic_authentication
      end
    else
      return redirect_to "https://www.odigo.jp#{request.path}"
    end

    # If the request 'HTTP_ACCEPT' header indicates a '*/*;q=0.6' format,
    # we set the format to :html.
    # This is necessary for GoogleBot which performs its requests with '*/*;q=0.6'
    # or similar HTTP_ACCEPT headers.
    if request.format.to_s !~ /html/
      request.format = :html
    end

    if redirect_spot_show_with_slug
      return
    end

    set_og_properties

    respond_to do |format|
      format.html
    end
  end

  # For short page to use
  # odigo.co/j12kh312
  def t
    short_id = params[:short_id].to_s.to_i 36
    ids = Trip.where(short_id: short_id, public: true).pluck :id
    if ids.first
      redirect_to "/trips/#{ids.first}"
    else
      raise Mongoid::Errors::DocumentNotFound.new(Trip, short_id: short_id)
    end
  end

  def profile_confirm
    flash.discard(:warning)
  end

  def trip_spot_list
    trip = Trip.find(params[:id])
    spots_hash = trip.all_spots.map do |spot|
      {
        id: spot.id.to_s,
        name: spot.name,
        average_duration: spot.average_duration_time,
        city_id: spot.city_id.to_s,
        coords: [spot.lat, spot.lng]
      }
    end
    render json: {
      spots: spots_hash,
      arrival_airport: "Haneda",
      leaving_airport: "kansai airport",
      daily_start_hour: 9,
      active_hours_per_day: 10,
      start_date: "2015-11-11"
    }
  end

  # just for development preview TODO move to admin
  # http://localhost:3000/trips/:id.pdf_preview?for=mobile&pp=skip&day=0
  def pdf_preview
    day_index = params[:day] ? params[:day].to_i(10) : 0
    trip = Trip.find params[:id]
    trip_day = trip.trip_days[day_index]
    unless trip_day
      raise "trip day not exist (start from 0)"
    end

    renderer = GuideBooksRenderer.new trip, "#{request.scheme}://#{request.host}:#{request.port}"
    case params[:for]
    when 'mobile'
      render html: renderer.for_mobile(trip_day).html_safe
    else
      render html: renderer.for_printing(trip_day).html_safe
    end
  end

  # just for development preview TODO move to admin
  # http://localhost:3000/trips/:id.map_preview
  # http://localhost:3000/trips/:id.map_preview?clear_cache&snapshoted
  def map_preview
    trip = Trip.find params[:id]

    if params.key?(:clear_cache)
      trip.trip_days.each do |d|
        d.map_cache.try :clear_snapshots
      end
    end

    if params.key?(:snapshoted)
      MapCache.ensure_map_snapshots trip
      trip = Trip.find trip.id
      map_data = MapCache.map_data trip, only_snapshoted: true
    else
      map_data = MapCache.map_data trip
    end

    render locals: {map_data: map_data}
  end

  protected

  def redirect_spot_show_with_slug
    case request.path
    when %r[^/spots/(\w+)([^/]*)], '/trips/new'
      spot_id = $1 || params[:popup_spot_id]
      slug = "#$1#$2"
    end

    if spot_id
      spot = PublicSpot.where(_id: spot_id, status: 'approved').first
      if spot and URI::encode(spot.slug) != slug
        redirect_to "/spots/#{spot.slug}", status: :moved_permanently
        true
      end
    end
  end

  def set_og_properties
    case request.path
    when %r[^/spots/(\w+)], '/trips/new'
      spot_id = $1 || params[:popup_spot_id]
      @og_properties = Rails.cache.fetch ["public_spots", spot_id, "og_properties"] do
        if spot = PublicSpot.where(_id: spot_id, status: 'approved').first
          {
            title: spot.name,
            image: spot.cover_image_url,
            url: url_for_path("/spots/#{spot_id}"),
            description: spot.desc_html
          }
        end
      end
    when %r[^(?:/my)?/trips/(\w+)$]
      trip_id = $1
      @og_properties = Rails.cache.fetch ["trips", trip_id, "og_properties"] do
        if trip = Trip.where(_id: trip_id, public: true).first
          {
            title: trip.title,
            image: trip.cached_cover_photo_url("big"),
            url: url_for_path("/trips/#{trip_id}"),
            description: trip.display_desc
          }
        end
      end
    end
  end

  def url_for_path path
    "#{request.scheme}://#{request.host}#{request.port == 80 ? '' : ":#{request.port}"}#{path}"
  end
end
