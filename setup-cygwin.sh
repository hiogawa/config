#!/bin/bash

# === README ===
# Setup by the following command:
# $ ln -sf $PWD/.bashrc-cygwin ~/.bashrc

# One time setup, Let audio playback app (eg vlc) to probe my music library
# $ mklink /d C:\Users\hiogawa-microsoft\Music\youtube C:\Users\hiogawa-microsoft\code\hiogawa\blob\music\youtube
# $ mklink /d C:\Users\hiogawa-microsoft\Documents\reading C:\Users\hiogawa-microsoft\code\hiogawa\blob\reading
