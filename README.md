```
# Linux

# bash
ln -sf $PWD/.bashrc ~/
ln -sf $PWD/.bashrc ~/.bashrc_profile

# git
ln -sf $PWD/.gitconfig ~/
ln -sf $PWD/.gitignore_global ~/

# vscode
ln -sf $PWD/vscode/settings.json ~/.config/Code\ -\ OSS/User/settings.json
ln -sf $PWD/vscode/keybindings.json ~/.config/Code\ -\ OSS/User/keybindings.json

ln -sf $PWD/vscode/settings.json ~/.config/Code\ -\ Insiders/User/settings.json
ln -sf $PWD/vscode/keybindings.json ~/.config/Code\ -\ Insiders/User/keybindings.json

# jupyter notebook keybindings
mkdir -p ~/.jupyter/nbconfig
ln -sf $PWD/notebook.json ~/.jupyter/nbconfig/notebook.json

# tmux
ln -sf "$PWD/.tmux.conf" "$HOME"

# mpd
mkdir -p "$HOME/.config/mpd"
ln -sf "$PWD/mpd.conf" "$HOME/.config/mpd"
```
