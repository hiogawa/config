#!/bin/bash

FILES="
    .bashrc .bash_profile
    .atom
    .emacs.d
    .gitconfig .gitignore_global
    .vimrc
    .tmux.conf
"

for file in $FILES; do
    rm -rf ~/$file # for directory (e.g. .atom)
    ln -sf $PWD/$file ~/
done

# cover .bash_profile
ln -sf $PWD/.bashrc ~/.bash_profile

# sway window manager
rm -rf ~/.config/sway
ln -sf $PWD/sway ~/.config/

# ranger config
rm -rf ~/.config/ranger
ln -sf $PWD/ranger ~/.config/
